<?php /*
Template Name: Contact Page
*/ ?>

<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<div id="contact" class="page-background full-width" style="background-image: url(<?php echo $src[0]; ?>);"> 
			<div id="blue-header" class="full-width">
				<h1><?php echo get_the_title( $ID ); ?></h1>
				<div id="page-breadcrumbs">  
					<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				</div>
			</div> 
			<div id="spacer" class="max-width"> 
			</div>
		</div>
		<div id="contact-row-one" class="full-width"> 
			<div class="max-width"> 
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<div style="clear: both"></div>
			</div> 
		</div>
		<div id="contact-row-two" class="full-width"> 
			<div class="max-width"> 
				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('contact-info')) : else : ?>
					<p><strong>Widget Ready</strong></p>  
				<?php endif; ?>  
				<div style="clear: both"></div>
			</div> 
			<div id="blue-overlay"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/blue-overlay.png" /></div>
			<div id="map-container"><iframe id="map-iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2933.0064866558714!2d-83.14720058430699!3d42.68240492267583!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8824e995c7a3a1b3%3A0xa8915cfd107cecc9!2s950+W+University+Dr+%23300%2C+Rochester%2C+MI+48307!5e0!3m2!1sen!2sus!4v1453136270124" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
		</div>
		<div id="contact-row-three" class="full-width"> 
			<?php echo do_shortcode('[slick-carousel-slider]'); ?>
		</div>

	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>