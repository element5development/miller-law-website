<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<div id="single-post" class="page-background full-width" style="background-image: url(<?php echo $src[0]; ?>);"> 
			<div id="blue-header" class="full-width">
				<h1><?php echo get_the_title( $ID ); ?></h1>
				<div id="page-breadcrumbs">  
					<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				</div>
			</div> 
			<div id="spacer" class="max-width"> 
			</div>
		</div>
		<div id="feed-row-one" class="full-width"> 
			<div id="feed-white-background"></div>
			<div class="max-width">
				<div id="single-display">  
					
					<?php
					// Start the loop.
					while ( have_posts() ) : the_post();

						// Include the single post content template.
						get_template_part( 'template-parts/content', 'single' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) {
							comments_template();
						}

						if ( is_singular( 'attachment' ) ) {
							// Parent post navigation.
							the_post_navigation( array(
								'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'twentysixteen' ),
							) );
						} elseif ( is_singular( 'post' ) ) { 
							// Previous/next post navigation. ?>
								<div id="previous-post" class="one-half">  
									<?php previous_post_link('%link', 'Previous Post', TRUE); ?>
								</div>
									<div id="next-post" class="one-half">  
										<?php next_post_link( '%link', 'Next Post', TRUE ); ?>
									</div>
								<div style="clear: both"></div>
							<?php 
						}

						// End of the loop.
					endwhile;
					?>
					<div style="clear: both"></div>
				</div>
				<div id="feed-sidebar">
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('feed-sidebar')) : else : ?>
						<p><strong>Widget Ready</strong></p>  
					<?php endif; ?>  
					<div style="clear: both"></div>
				</div>
				<div style="clear: both"></div>
			</div>
		</div>
	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>