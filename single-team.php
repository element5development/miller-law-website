<?php
get_header( );

global $TLPteampro, $post;
while ( have_posts() ) : the_post();
	$designation = strip_tags(get_the_term_list(get_the_ID(), $TLPteampro->taxonomies['designation'], null, ','));
?>
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<div id="single-attorney" class="page-background full-width"> 
			<div id="blue-header" class="full-width">
				<h1><?php the_title(); ?></h1>
				<?php
					$email = get_post_meta( $post->ID, 'email', true );
					$telephone = get_post_meta( $post->ID, 'telephone', true );

					$html .= '<div class="intro-contact-info">';
						if($telephone){
							$html.='<a href="tel:'.$telephone.'"><div class="intro-phone"><i class="fa fa-phone"></i> <span>'.$telephone.'</span></div></a>';
						}
						if($email){
							$html.='<a href="mailto:'.$email.'"><div class="intro-email"><i class="fa fa-envelope-o"></i> <span>'.$email.'</span> </div></a>';
						}
					$html .= '<div style="clear: both"></div></div>';

				echo $html;
				?>
				<div id="page-breadcrumbs">  


					<p id="breadcrumbs"><span xmlns:v="http://rdf.data-vocabulary.org/#"><span typeof="v:Breadcrumb"><a href="http://millerlawpc.com" rel="v:url" property="v:title">Home</a>  <span rel="v:child" typeof="v:Breadcrumb"><a href="/attorneys/" rel="v:url" property="v:title">Attorneys</a>   <span class="breadcrumb_last"><?php the_title();?></span></span></span></p>


				</div>
			</div> 
			<div id="spacer" class="max-width"> 
			</div>
		</div>
		<div id="feed-row-one" class="full-width attorney-single"> 
			<div id="single-attorney-content" class="max-width"> 
				<div id="single-attorney-info">  
					<div id="single-attorney-image">  
						<?php 
							$s = unserialize(get_post_meta( get_the_ID(), 'social' , true));

							if ( has_post_thumbnail() ) { the_post_thumbnail('large'); } 
							if($s){
								$social .= '<div class="social-icons">';
								foreach ($s as $sID) {
									$social .= "<a class='fa fa-{$sID['id']}' href='{$sID['url']}' title='{$sID['id']}' target='_blank'><i class='fa fa-{$sID['id']}'></i></a>";
								}
								$social .="</div>";
							}
						echo $social;
						?>
					</div>
					<div id="single-attorney-name">  
						<h3><?php the_title(); ?></h3>
						<?php if($designation){ echo '<div class="tlp-position">'.$designation.'</div>'; }?>
					</div>
					<div id="single-attorney-contact">
						<?php
						$email = get_post_meta( $post->ID, 'email', true );
						$telephone = get_post_meta( $post->ID, 'telephone', true );
						$web_url = get_post_meta( $post->ID, 'web_url', true );
						$short_bio = get_post_meta( $post->ID, 'short_bio', true );
						$html = null;
						$html .="<div class='tlp-team'>";
						$html .= '<div class="contact-info">';
						$html .= '<ul>';
							if($email){
								$html.='<a href="mailto:'.$email.'"><li class="tlp-email"><i class="fa fa-envelope-o"></i> <span>'.$email.'</span> </li></a>';
							}
							if($telephone){
								$html.='<a href="tel:'.$telephone.'"><li class="tlp-phone"><i class="fa fa-phone"></i> <span>'.$telephone.'</span></li></a>';
							}
							if($web_url){
								$html.='<a href="tel:'.$web_url.'"><li class="tlp-location"><i class="fa fa-map-marker"></i>  <span>'.$web_url.'</span> </li></a>';
							}
						$html .= '</ul>';
						$html .= "</div>";
						$html .= '</div>';
        
						echo $html;
						?>

						<div class="short-bio">
							<h2>Education</h2>
							<p><?php the_field( 'team_education' ) ?></p>
						</div>
					</div>
				</div>
				<div id="single-attorney-bio">  
					<?php the_content(); ?>
				</div>
				<div style="clear: both"></div>
			</div>
		</div>
		<div id="feed-row-two" class="full-width attorney-single"> 
			<div id="single-attorney-posts" class="max-width">  
				<?php
					$author_name = str_replace(' ', '', get_the_title());
					$query = new WP_Query( array( 'author_name' => $author_name, 'showposts' => 3 ) ); 
				 ?>
				<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

				<div class="post">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
						<div id="post-image">  
							<?php the_post_thumbnail( '' ); ?>
							<div id="feed-hover"></div>
						</div>
					</a>
					<div id="post-content">
						<small><?php the_time( 'F jS, Y' ); ?></small>
						<!-- Display the Title as a link to the Post's permalink. -->
						<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
						<div class="entry">
							<p><?php echo substr(strip_tags($post->post_content), 0, 150)."...";?></p>
							<a href="<?php the_permalink() ?>" class="read-article">Read this Article</a>
						</div>
					</div>
					<div style="clear: both"></div>
				</div> <!-- closes the first div box -->

				 <?php endwhile; 
				 wp_reset_postdata();
				 else : ?>

				 <?php endif; ?>
			</div>
			<div style="clear: both"></div>
		</div>

	</div>
</div>
<?php endwhile;
get_footer();