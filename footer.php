<?php
/**
 * The template for displaying the footer
 * Contains footer content and the closing of the #main and #page div elements.
 */
?>

			</div><!-- #main -->

		</div><!-- #page -->
			
	<!--Custom Footer-->
	<footer>
		<div class="max-width">
			<div class="footer-row"> 
				<img id="footer-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/Miller-Law-Logo-Small-White.svg" alt="Miller Law" />
				<p id="footer-quote">Changing the Odds in Your Favor</p>
				<div style="clear: both"></div>
			</div>
			<div class="footer-row">
				<div id="footer-contact" class="one-third">  
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('footer-column-one')) : else : ?>
						<p>Widget Ready</p>  
					<?php endif; ?>  
				</div>
				<div id="footer-links" class="one-third">
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('footer-column-two')) : else : ?>
						<p>Widget Ready</p>  
					<?php endif; ?>    
				</div>
				<div id="footer-connect" class="one-third"> 
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('footer-column-three')) : else : ?>
						<p>Widget Ready</p>  
					<?php endif; ?>  
				</div>
				<div style="clear: both"></div>
			</div>
		</div>
		<div id="copyright"> 
			<p class="max-width"><span>© Copyright <?php echo date('Y'); ?> The Miller Law Firm. All Rights Reserved. <a target="_blank" href="https://element5digital.com/">Web Design by Element5</a>.</span><a href="/privacy-policy/">Privacy Policy</a></p>
		</div>
	</footer>
	<?php wp_footer(); ?>
<!-- Coverflow Slider -->	
	<?php if(is_page( 9 )) { ?>
		<script src="<?php bloginfo('stylesheet_directory'); ?>/JS/jquery.flipster.min.js"></script>
		<script>
			var carousel = jQuery("#jd-carousel").flipster({
				style: 'carousel',
				spacing: -.5,
				nav: true,
				buttons: true,
				loop: true,
			});
		</script>
	<?php } ?>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/JS/slick.js"></script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/JS/slick-init.js"></script>
</body>
</html>