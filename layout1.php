<?php
/*
* Layout 1: On mouse hover social and detail icon 
*
   Variable
   $tlp_active_link,$mID(post ID),$title,$pLink,$imgSrc
   field
   $fname,$fdesignation
*
*/




/*
    IF TLP TEAM PRO PLUGIN UPDATED REUPLOAD THIS FILE TO THE PLUGIN
    tlp-team-pro > lib > views > layout
*/
$html = null;
$html .="<div class='tlp-col-lg-{$grid} tlp-col-md-{$grid} tlp-col-sm-6 tlp-col-xs-12 tlp-equal-height {$margin}'>";
    $html .='<div class="single-team-area">'; 
        $html .='<div class="single-team">'; 
        if ($imgSrc){
        $html .='<figure>';
            $html .='<img class="img-responsive" src="'.$imgSrc.'" alt="'.$title.'"/>';
        $html .='</figure>';
        }
    $html .='<figcaption>';
        $html .='<div class="overlay">'; 
            $html .='<div class="overlay-element">'; 
            if($tlp_active_link=='yes'){
                $html .='<span class="detail-link"><a class="'.$popup_class.' detail-popup" data-id="'.$mID.'" href="'.$pLink.'"><i class="fa fa-plus"></i><p>Learn More</p></a></span>';
            }
            $html .= '<div class="social-icons">';
            if(!empty($sLink) && is_array($sLink) && $fsocial){
                foreach ($sLink as $id => $link) {
                    @$html .= "<a href='{$link['url']}' title='{$link['id']}' target='_blank'><i class='fa fa-{$link['id']}'></i></a>";
                }
            }  
            $html .= '</div>';
        $html .= '</div>';
     $html .= '</div>';
    $html .= '</figcaption>';
   $html .= '</div>';

    if($fname && $title){
       $html .= '<div class="tlp-content">';
            if($tlp_active_link=='yes'){
                $html .= '<h3><span class="team-name"><a class="'.$popup_class.'" data-id="'.$mID.'" title="'.$title.'" href="'.$pLink.'">'.$title.'</a></span></h3>';
            }else{
                $html .= '<h3><span class="team-name">'.$title.'</span></h3>';
            }
           if($designation && $fdesignation){
                $html .= '<div class="tlp-position">'.$designation.'</div>';
            }
        $html .= '</div>';
        }
        
        if($short_bio && $fshort_bio){
            $html .= '<div class="short-bio"><p>'.$short_bio.'</p></div>';
        }
        
        if(($femail)||($ftelephone)||($flocation)||($fweb_url)){
            $html .='<div class="contact-info">';  
                $html .='<ul>';
                    if($ftelephone && $telephone){
                        $html.='<li><i class="fa fa-phone"></i><span class="tlp-phone">'.$telephone.'</span></li>';
                    }
                    if($femail && $email){
                        $parts = explode(' ', $title); // $meta->post_title
                        $name_first = array_shift($parts);
                        $name_last = array_pop($parts);
                        $name_middle = trim(implode(' ', $parts));

                        if($name_first == 'E.'){
                            $name_first = 'Powell';
                        }

                       $html .='<li><i class="fa fa-envelope-o"></i><a href="mailto:'.$email.'"><span class="tlp-email">Contact '.$name_first.'</span></a></li>';
                    }
                    if($flocation && $location){
                        $html.='<li><i class="fa fa-map-marker"></i><span class="tlp-location">'.$location.'</span></li>';
                    }
                    if($fweb_url && $web_url){
                        $html.='<li><a target="_blank" href="'.$web_url.'"><i class="fa fa-globe"></i><span class="tlp-url">'.$web_url.'</span></a></li>';
                    }
                $html .='</ul>';
            $html .='</div>';
        }

        if($tlp_skill && is_array($tlp_skill) && $fskill){
            $html.='<div class="tlp-team-skill">';
            foreach ($tlp_skill as $id => $skill)
            {
                $html .= "<div class='skill_name'>{$skill['id']}</div><div class='skill-prog tlp-tooltip' title='{$skill['percent']}%'><div class='fill' data-progress-animation='{$skill['percent']}%'></div></div>";
            }
            $html .= '</div>';
        }
    $html .='</div>'; 
$html .='</div>'; 
return $html;