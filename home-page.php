<?php /*
Template Name: Home Page
*/ ?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<div id="video-intro" class="page-background full-width"> 
			<div class="videowrap">
				<div class="gradient">
					<div class="overlay">
						<video muted="" autoplay="" loop="" poster="<?php bloginfo('stylesheet_directory'); ?>/img/transparent.png" class="bgvid" id="videoBG">
						</video>
					</div>
				</div>
			</div>
			<div id="intro-header" class="max-width">
				<div id="intro-fade"></div>
				<h1><span>Leaders in Complex Business Lawsuits</span></br>and Class Action Litigation</h1>
				<div style="clear: both"></div>
			</div> 
			<div id="intro-info" class="full-width">
				<div id="info-container" class="full-width">
					<div class="max-width">  
						<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>
								<?php the_content(); ?>
							<?php endwhile; ?>
						<?php endif; ?>
						<p><a href="#home-row-one" class="smoothScroll"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/down-arrow.svg" /></a></p>
					</div>
				</div>
			</div>
		</div>
		<div id="home-row-one" class="full-width">  
			<div class="one-half">  
				<div id="attorney-search">
					<h1 class="dark-blue-font">Search for Attorneys</h1>
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('attorney-search')) : else : ?>
						<p><strong>Widget Ready</strong></p>  
					<?php endif; ?>  
					<form role="search" method="get" id="searchform"
					    class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					    <div>
					        <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
					        <input type="text" placeholder="Search by name..." name="s" id="s" />
					        <input type="hidden" name="post_type" value="team" />
					        <input type="submit" id="searchsubmit"
					            value="Search" />
					    </div>
					</form>
					<div style="clear: both"></div>
				</div>
			</div>
			<div style="clear: both"></div>
		</div>
		<div id="home-row-two" class="full-width">  
			<div class="one-half">  
				<div id="recent-settlements">
					<h1 class="white-font">Recent Case Results</h1>
					<?php echo do_shortcode('[widget id="tw-recent-posts-3"]'); ?>
				</div>
			</div>
			<div style="clear: both"></div>
		</div>
		<div id="home-row-three" class="full-width">  
			<div class="one-half">  
				<div id="recent-blog">
					<h1 class="dark-blue-font">Latest News</h1>
					<?php echo do_shortcode('[widget id="tw-recent-posts-2"]'); ?>
				</div>
			</div>
			<div style="clear: both"></div>
		</div>
		<div id="home-row-four" class="full-width">  
			<div class="max-width"> 
				<h1 class="white-font">Case Results</h1>
				<?php get_template_part( 'template-parts/case-results-slider' ); ?>
			</div>
		</div>
		<div id="home-row-five" class="full-width">  
			<div class="max-width"> 
				<h1 class="dark-blue-font">Awards</h1> 
				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('home-awards')) : else : ?>
						<p><strong>Widget Ready</strong></p>  
				<?php endif; ?> 
				<div style="clear: both"></div>
			</div>
		</div>

	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>