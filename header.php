<?php
/**
 * The Header template for our theme
 * Displays all of the <head> section and everything up till <div id="main">
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-75676224-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-75676224-1');
</script>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<!-- Mobile Site Media Query -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSS data -->
	<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet" type="text/css" />
<!-- Smooth Scrolling -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/JS/smoothscroll.js"></script>
<!-- Sticky Nav -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/JS/stickynav.js"></script>
<!-- Entrance Animations -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/JS/entranceanimation.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/JS/jquery.viewportchecker.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/animate.css" />
<!-- Slick Slider -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/slick-theme.css"/>
<!-- Enable Map -->
	<script type="text/javascript">
		jQuery(document).ready(function(){
			$('#blue-overlay').click(function(){
				$('#blue-overlay').css('pointer-events','none')
			    $('#map-iframe').css('pointer-events','auto');
			});
		})
	</script>
<!-- Coverflow Slider -->	
	<?php if(is_page( 9 )) { ?>
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/jquery.flipster.min.css" />
	<?php } ?>
<!-- Random Video Background -->
	<?php if(is_page( 4 )) { ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				$vidnumber = Math.floor((Math.random() * 6) + 1);

				if($vidnumber == 1) {
					document.getElementById("videoBG").innerHTML = '<source src="<?php bloginfo('stylesheet_directory'); ?>/img/video1.mp4" type="video/mp4"><source src="<?php bloginfo('stylesheet_directory'); ?>/img/video1.webm" type="video/webm">';
				}
				else if($vidnumber == 2) {
					document.getElementById("videoBG").innerHTML = '<source src="<?php bloginfo('stylesheet_directory'); ?>/img/video2.mp4" type="video/mp4"><source src="<?php bloginfo('stylesheet_directory'); ?>/img/video2.webm" type="video/webm">';
				}
				else if($vidnumber == 3) {
					document.getElementById("videoBG").innerHTML = '<source src="<?php bloginfo('stylesheet_directory'); ?>/img/video3.mp4" type="video/mp4"><source src="<?php bloginfo('stylesheet_directory'); ?>/img/video3.webm" type="video/webm">';
				}
				else if($vidnumber == 4) {
					document.getElementById("videoBG").innerHTML = '<source src="<?php bloginfo('stylesheet_directory'); ?>/img/video4.mp4" type="video/mp4"><source src="<?php bloginfo('stylesheet_directory'); ?>/img/video4.webm" type="video/webm">';
				}
				else if($vidnumber == 5) {
					document.getElementById("videoBG").innerHTML = '<source src="<?php bloginfo('stylesheet_directory'); ?>/img/video5.mp4" type="video/mp4"><source src="<?php bloginfo('stylesheet_directory'); ?>/img/video5.webm" type="video/webm">';
				}
				else {
					document.getElementById("videoBG").innerHTML = '<source src="<?php bloginfo('stylesheet_directory'); ?>/img/video6.mp4" type="video/mp4"><source src="<?php bloginfo('stylesheet_directory'); ?>/img/video6.webm" type="video/webm">';
				}
			})
		</script>
	<?php } ?>

</head>

<body <?php body_class(); ?>>
	<header id="main-nav" class="max-width">
		<div id="top-menu">
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('header-top-menu')) : else : ?>
				<p>Widget Ready</p>  
			<?php endif; ?>  
		</div>
		<div style="clear: both"></div>
		<a href="/"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/Miller-Law-Logo.svg" alt="Miller Law" /></a>
		<nav>
			<?php 
		        $defaults = array(
		            'theme_location'  => 'main-menu',
		            'after'           => '<div id="menu-filler"></div>',
		        );
		        wp_nav_menu($defaults);
		    ?>

		</nav> 
		<div style="clear: both"></div>
	</header>
	<header id="sticky-nav" class="full-width">
		<div id="top-menu">
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('header-top-menu')) : else : ?>
				<p>Widget Ready</p>  
			<?php endif; ?>  
		</div>
		<div style="clear: both"></div>
		<div class="full-width">
			<div class="max-width">
				<a href="/"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/Miller-Law-Sticky-Logo.svg" alt="Miller Law" /></a>
				<nav>
					<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
				</nav> 
				<div style="clear: both"></div>
			</div>
		</div>
	</header>
	<div id="page" class="hfeed site">
			<div id="main" class="site-main">
