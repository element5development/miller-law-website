<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div id="search-results" class="page-background full-width"> 
				<div id="blue-header" class="full-width">
					<h1><?php printf( __( 'Search Results for: %s', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
					<div id="page-breadcrumbs">  
						<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
					</div>
				</div> 
				<div id="page-content" class="max-width"> 
					<h2>Scroll down to view results or try another search.</h2>
					<?php echo do_shortcode('[widget id="search-3"]'); ?>
					<p><a href="#feed-row-one" class="smoothScroll"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/down-arrow.svg" /></a></p>
				</div>
			</div>
			<div id="feed-row-one" class="full-width"> 
				<div class="max-width">
					<div id="search-feed">  
						
						<?php if ( have_posts() ) : ?>

							<?php
							// Start the loop.
							while ( have_posts() ) : the_post();

								/**
								 * Run the loop for the search to output the results.
								 * If you want to overload this in a child theme then include a file
								 * called content-search.php and that will be used instead.
								 */
								get_template_part( 'template-parts/content', 'search' );

							// End the loop.
							endwhile;

							// Previous/next page navigation.
							the_posts_pagination( array(
								'prev_text'          => __( 'Previous page', 'twentysixteen' ),
								'next_text'          => __( 'Next page', 'twentysixteen' ),
								'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
							) );

						// If no content, include the "No posts found" template.
						else : ?>
							<article>
								<h2>Nothing Found Try Again</h2>
								<?php echo do_shortcode('[widget id="eps_super_search-3"]'); ?>
							</article>

						<?php endif; ?>
						
					</div>
					<div style="clear: both"></div>
				</div>
			</div>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
