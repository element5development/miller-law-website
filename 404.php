<?php /*
The template for displaying 404 pages (broken links)
*/ ?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<div id="default" class="page-background full-width" style="background-image: url(/wp-content/uploads/2016/01/about-background.png);"> 
			<div id="blue-header" class="full-width">
				<h1>The Page Your Looking For Has Been Lost</h1>
				<div id="page-breadcrumbs">  
					<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				</div>
			</div> 
			<div id="spacer" class="max-width"> 
			</div>
		</div>
		<div id="basic-content-bg" class="full-width">
			<h2 class="error-message">Try Searching Our Site or One of the Pages Listed Below!</h2>
			<div id="basic-content" class="max-width"> 
				<section class="error-404 not-found">
					<div class="error-search-form"><?php get_search_form(); ?></div>
					<div class="error-sitemap-menu">
						<?php wp_nav_menu( array( 'theme_location' => 'error-menu' ) ); ?> 
					</div>
				</section>
			</div>
		</div>

	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>
