<?php

//Pull Parent Styles
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

// Allow SVG Files to WordPress Media
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// ADD JQUERY TO WORDPRESS
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js", false, null);
   wp_enqueue_script('jquery');
}

// btn shortcode
add_filter('widget_text', 'do_shortcode');
function transparent_button($atts, $content = null) {
    extract( shortcode_atts( array(
        'url' => '#'
    ), $atts ) );
    return '<a href="'.$url.'" class="transparent-button">' . do_shortcode($content) . '</a>';
}
add_shortcode('transparent-btn', 'transparent_button');
function solid_button($atts, $content = null) {
    extract( shortcode_atts( array(
        'url' => '#'
    ), $atts ) );
    return '<a href="'.$url.'" class="solid-button">' . do_shortcode($content) . '</a>';
}
add_shortcode('solid-btn', 'solid_button');
function pdf_button($atts, $content = null) {
    extract( shortcode_atts( array(
        'url' => '#'
    ), $atts ) );
    return '<a href="'.$url.'" target="_blank" class="pdf-button">' . do_shortcode($content) . '</a>';
}
add_shortcode('pdf-btn', 'pdf_button');

// Remove Parent Widget Areas
function remove_parent_theme_widgets(){
    // Duplicate lines with the name of the sidebars
    unregister_sidebar( 'sidebar-1' );
    unregister_sidebar( 'sidebar-2' );
    unregister_sidebar( 'sidebar-3' );
}
add_action( 'widgets_init', 'remove_parent_theme_widgets', 11 );

// Limit Archives Month to Display
function my_limit_archives( $args ) {
    $args['limit'] = 10;
    return $args;
}
add_filter( 'widget_archives_args', 'my_limit_archives' );
add_filter( 'widget_archives_dropdown_args', 'my_limit_archives' );

// ALLOW EDITOR ACCESS TO GRAVITY FORMS, MENUS & WIDGETS
    function add_grav_forms(){
        $role = get_role('editor');
        $role->add_cap('gform_full_access');
        $role->add_cap('edit_theme_options');
        $role->add_cap('list_users');
    }
    add_action('admin_init','add_grav_forms');

// Add Widget Areas
if ( ! function_exists( 'custom_sidebar' ) ) {
 
    // Register Sidebar
    function custom_sidebar() {
        //copy code for each widget area 
        $args = array(
            'id'            => 'header-top-menu',
            'name'          => __( 'Header Top Menu', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'header-top-menu',
            'before_title'  => '<h3 class="top-menu-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
        $args = array(
            'id'            => 'attorney-search',
            'name'          => __( 'Attorney Search', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'home-row-one',
            'before_title'  => '<h3 class="home-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
        $args = array(
            'id'            => 'home-awards',
            'name'          => __( 'Home Awards', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'home-row-four',
            'before_title'  => '<h3 class="home-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
        $args = array(
            'id'            => 'about-columns',
            'name'          => __( 'About Columns', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'about-row-one',
            'before_title'  => '<h3 class="home-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
        $args = array(
            'id'            => 'practice-areas',
            'name'          => __( 'Practice Areas', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'practice-row-one',
            'before_title'  => '<h3 class="home-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
        $args = array(
            'id'            => 'practice-child-sidebar',
            'name'          => __( 'Practice Area Child Page Sidebar', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'practice-child-sidebar',
            'before_title'  => '<h3 class="practice-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
        $args = array(
            'id'            => 'publications',
            'name'          => __( 'Publications', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'publications-row-one',
            'before_title'  => '<h3 class="home-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
        $args = array(
            'id'            => 'contact-info',
            'name'          => __( 'Contact Info', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'contact-info',
            'before_title'  => '<h3 class="contact-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
        $args = array(
            'id'            => 'feed-sidebar',
            'name'          => __( 'Feed Sidebars', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'feed-sidebar',
            'before_title'  => '<h3 class="home-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
        $args = array(
            'id'            => 'footer-column-one',
            'name'          => __( 'Footer Column One', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'footer-column-one',
            'before_title'  => '<h3 class="footer-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
        $args = array(
            'id'            => 'footer-column-two',
            'name'          => __( 'Footer Column Two', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'footer-column-two',
            'before_title'  => '<h3 class="footer-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
        $args = array(
            'id'            => 'footer-column-three',
            'name'          => __( 'Footer Column Three', 'twentysixteen' ),
            'description'   => __( ' ', 'twentysixteen' ),
            'class'         => 'footer-column-three',
            'before_title'  => '<h3 class="footer-title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
        );
        register_sidebar( $args );
    }
add_action( 'widgets_init', 'custom_sidebar' );
}

// Add Main Menu
register_nav_menu('Main-Menu','Header Menu');
// Add Main Menu
function register_menu () {
    register_nav_menu('main-menu', __('Main Menu'));
    register_nav_menu('error-menu', __('404 Menu'));
}
add_action('init', 'register_menu');

//SEARCH ONLY POST TITLES
function __search_by_title_only( $search, &$wp_query )
{
    global $wpdb;
    if(empty($search)) {
        return $search; // skip processing - no search term in query
    }
    $q = $wp_query->query_vars;
    $n = !empty($q['exact']) ? '' : '%';
    $search =
    $searchand = '';
    foreach ((array)$q['search_terms'] as $term) {
        $term = esc_sql($wpdb->esc_like($term));
        $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
        $searchand = ' AND ';
    }
    if (!empty($search)) {
        $search = " AND ({$search}) ";
        if (!is_user_logged_in())
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }
    return $search;
}
add_filter('posts_search', '__search_by_title_only', 500, 2);


?>