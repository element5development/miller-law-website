<?php /*
Template Name: Practice Areas Page
*/ ?>

<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<div id="practice" class="page-background full-width" style="background-image: url(<?php echo $src[0]; ?>);"> 
			<div id="blue-header" class="full-width">
				<h1><?php echo get_the_title( $ID ); ?></h1>
				<div id="page-breadcrumbs">  
					<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				</div>
			</div> 
			<div id="page-content" class="max-width"> 
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<p><a href="#practice-row-one" class="smoothScroll"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/down-arrow.svg" /></a></p>
				</div>
			</div>
		</div>
		<div id="practice-row-one" class="full-width">   
			<div id="practice-areas" class="max-width">
				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('practice-areas')) : else : ?>
					<p><strong>Widget Ready</strong></p>  
				<?php endif; ?>  
				<div style="clear: both"></div>
			</div>
			<div style="clear: both"></div>
		</div>
		<div id="home-row-four" class="full-width">  
			<div class="max-width"> 
				<h1 class="white-font">Case Results</h1>
				<?php get_template_part( 'template-parts/case-results-slider' ); ?>
			</div>
		</div>
		<div id="practice-row-three" class="max-width">  
				<h1 class="dark-blue-font">Search for Attorneys</h1>
				<p>Start typing in an attorney’s name. The names will auto-complete after the first two letters.</p>
				<form role="search" method="get" id="searchform"
				    class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				    <div>
				        <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
				        <input type="text" placeholder="Search by name..." name="s" id="s" />
				        <input type="hidden" name="post_type" value="team" />
				        <input type="submit" id="searchsubmit"
				            value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" />
				    </div>
				</form>
		</div>
	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>