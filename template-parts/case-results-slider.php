<?php $catquery = new WP_Query( 'cat=8&posts_per_page=5' ); ?>
<div class="case-slider">

<?php while($catquery->have_posts()) : $catquery->the_post(); ?>

<div><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></div>
<?php endwhile; ?> 
<?php wp_reset_postdata(); ?>

</div>