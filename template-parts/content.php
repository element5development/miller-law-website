<?php /*
The template part for displaying content
*/ ?>

<article id="post-<?php the_ID(); ?>">
	<div class="post">
		<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
			<div id="post-image">  
				<?php the_post_thumbnail( 'thumbnail' ); ?>
				<div id="feed-hover"></div>
			</div>
		</a>
		<div id="post-content">
			<small><?php the_time( 'F jS, Y' ); ?></small>
			<!-- Display the Title as a link to the Post's permalink. -->
			<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
			<div class="entry">
				<?php the_field( 'intro_text' ) ?>
			</div>
		</div>
		<div style="clear: both"></div>
	</div> <!-- closes the first div box -->
</article><!-- #post-## -->
