<?php /*
The template part for displaying results in search pages
*/ ?>

<article id="post-<?php the_ID(); ?>" class="post-feed search-feed animated">

	<div class="post-feed-cat"><?php the_category(', ') ?></div>

	<header class="entry-header">
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
			<span class="sticky-post"><?php _e( 'Featured', 'twentysixteen' ); ?></span>
		<?php endif; ?>

		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->

	<?php twentysixteen_post_thumbnail(); ?>

	<div class="post-feed-excerpt"><?php the_excerpt(); ?></div> <!-- Pull Post Excerpt -->

	<div class="post-feed-image-overaly"></div>

	<div style="clear: both"></div>

</article><!-- #post-## -->


