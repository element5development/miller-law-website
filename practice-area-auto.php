<?php /*
Template Name: Automotive Supplier Page
*/ ?>

<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<div id="practice-child" class="page-background full-width" style="background-image: url(<?php echo $src[0]; ?>);"> 
			<div id="blue-header" class="full-width">
				<h1><?php echo get_the_title( $ID ); ?></h1>
				<div id="page-breadcrumbs">  
					<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				</div>
			</div> 
			<div id="page-content" class="max-width"> 
					<?php echo do_shortcode('[widget id="really-simple-image-widget-18"]'); ?>
					<p><a href="#practice-child-row-one" class="smoothScroll"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/down-arrow.svg" /></a></p>
					<div style="clear: both"></div>
				</div>
			</div>
		</div>
		<div id="practice-child-row-one" class="full-width"> 
			<div id="feed-white-background"></div>
			<div class="max-width">
				<div id="practice-child-content">  
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<div id="practice-child-sidebar">
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('practice-child-sidebar')) : else : ?>
						<p><strong>Widget Ready</strong></p>  
					<?php endif; ?>  
					<div style="clear: both"></div>
				</div>
				<div style="clear: both"></div>
			</div>
		</div>
	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>