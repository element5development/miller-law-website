/* Entrance Animations */

jQuery(document).ready(function() {
    jQuery('.widget_tr_ps_widget').addClass("hiddens").viewportChecker({
        classToAdd: 'visible animated zoomIn',
        offset: 100
       });
});

jQuery(document).ready(function() {
    jQuery('#practice-row-one .widget').addClass("hiddens").viewportChecker({
        classToAdd: 'visible animated fadeIn',
        offset: 100
       });
});

jQuery(document).ready(function() {
    jQuery('#feed-display .post').addClass("hiddens").viewportChecker({
        classToAdd: 'visible animated fadeIn',
        offset: 100
       });
});

jQuery(document).ready(function() {
    jQuery('#search-feed article').addClass("hiddens").viewportChecker({
        classToAdd: 'visible animated fadeIn',
        offset: 100
       });
});