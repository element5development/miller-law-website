<?php /*
The template for displaying archive pages

If you'd like to further customize these archive views, you may create a
new template file for each one. For example, tag.php (Tag archives),
category.php (Category archives), author.php (Author archives), etc.
*/ ?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<div id="news-press" class="page-background full-width" style="background-image: url(/wp-content/uploads/2016/01/about-background.png);"> 
			<div id="blue-header" class="full-width">
				<h1>
					<?php single_month_title(' ') ?>
					<?php single_cat_title(' '); ?>
				</h1>
				<div id="page-breadcrumbs">  
					<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				</div>
			</div> 
			<div id="spacer" class="max-width"> 
			</div>
		</div>
		<div id="feed-row-one" class="full-width"> 
			<div id="feed-white-background"></div>
			<div class="max-width">
				<div id="feed-display">  

					<?php if ( have_posts() ) : ?>
					<?php 
					while ( have_posts() ) : the_post();
						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', get_post_format() );
					endwhile; ?>
					<div style="clear: both"></div>
					<?php the_posts_pagination( array(
						'prev_text'          => __( 'Previous page', 'twentysixteen' ),
						'next_text'          => __( 'Next page', 'twentysixteen' ),
						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
					) );
					else : ?>
						<article>
							<h2>Nothing Found</h2>
						</article>
					<?php endif; ?>

				</div>
				<div id="feed-sidebar">
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('feed-sidebar')) : else : ?>
						<p><strong>Widget Ready</strong></p>  
					<?php endif; ?>  
					<div style="clear: both"></div>
				</div>
				<div style="clear: both"></div>
			</div>
		</div>
	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>