<?php /*
Template Name: Publications Page
*/ ?>

<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<div id="publications" class="page-background full-width" style="background-image: url(<?php echo $src[0]; ?>);"> 
			<div id="blue-header" class="full-width">
				<h1><?php echo get_the_title( $ID ); ?></h1>
				<div id="page-breadcrumbs">  
					<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				</div>
			</div> 
			<div id="page-content" class="max-width"> 
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<p><a href="#publications-row-one" class="smoothScroll"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/down-arrow.svg" /></a></p>
				</div>
			</div>
		</div>
		<div id="publications-row-one" class="full-width"> 
			<div class="max-width"> 
				<div style="padding: 50px 0;"><?php the_field( 'intro_text' ) ?></div>
				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('publications')) : else : ?>
					<p><strong>Widget Ready</strong></p>  
				<?php endif; ?>  
				<div style="clear: both"></div>
			</div> 
		</div>
	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>