<?php /*
Template Name: News & Press Page
*/ ?>

<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<div id="news-press" class="page-background full-width" style="background-image: url(<?php echo $src[0]; ?>);"> 
			<div id="blue-header" class="full-width">
				<h1><?php echo get_the_title( $ID ); ?></h1>
				<div id="page-breadcrumbs">  
					<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				</div>
			</div> 
			<div id="page-content" class="max-width"> 
					<?php echo category_description( 13 ); ?>
					<p><a href="#feed-row-one" class="smoothScroll"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/down-arrow.svg" /></a></p>
			</div>
		</div>
		<div id="feed-row-one" class="full-width news-press"> 
			<div id="feed-white-background"></div>
			<div class="max-width">
				<div id="feed-display">  
					<?php $query = new WP_Query( 'cat= 13' ); ?>
					<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<div class="post">
						<div id="post-content">
							<small><?php the_time( 'F jS, Y' ); ?></small>
							<div class="entry">
								<?php the_content(); ?>
							</div>
						</div>
						<div style="clear: both"></div>
					</div> <!-- closes the first div box -->

					 <?php endwhile; 
					 wp_reset_postdata();
					 else : ?>
					 <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
					 <?php endif; ?>
				</div>
				<div id="feed-sidebar">
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('feed-sidebar')) : else : ?>
						<p><strong>Widget Ready</strong></p>  
					<?php endif; ?>  
					<div style="clear: both"></div>
				</div>
				<div style="clear: both"></div>
			</div>
		</div>
	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>