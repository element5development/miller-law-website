<?php /*
Template Name: About Page
*/ ?>

<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<div id="about" class="page-background full-width" style="background-image: url(<?php echo $src[0]; ?>);"> 
			<div id="blue-header" class="full-width">
				<h1><?php echo get_the_title( $ID ); ?></h1>
				<div id="page-breadcrumbs">  
					<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				</div>
			</div> 
			<div id="page-content" class="max-width"> 
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<p><a href="#about-row-one" class="smoothScroll"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/down-arrow.svg" /></a></p>
				</div>
			</div>
		</div>
		<div id="about-row-one" class="full-width">   
			<div id="history-community-experience" class="max-width">
				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('about-columns')) : else : ?>
					<p><strong>Widget Ready</strong></p>  
				<?php endif; ?>  
				<div style="clear: both"></div>
			</div>
			<div style="clear: both"></div>
		</div>
		<div id="about-row-two" class="full-width">   
			<div id="recent-settlements" class="max-width">
				<h1 class="white-font">Case Results</h1>
				<?php get_template_part( 'template-parts/case-results-slider' ); ?>
			</div>
			<div style="clear: both"></div>
		</div>
		<!--COVERFLOW CONTAINER START-->
		<?php
			$coverflow_title = get_field( "field_56a22aff2f65f", 9 );
			$coverflow_background_image = get_field( "field_56a22b162f660", 9 );
		?>
		<div id="about-row-three" class="full-width" style="background-image: url(<?php echo ( !empty($coverflow_background_image)? $coverflow_background_image : '' ); ?>);">  
			<div id="community-involvement" class="max-width">
				<h1><?php echo ( !empty($coverflow_title)? $coverflow_title : '' ); ?></h1>
				<!--COVERFLOW SLIDER-->
				<div id="jd-carousel">
					<ul class="flip-items">
						<?php 
							for ($x = 0; $x <= 6; $x++) {
								$get_slider_image = get_field( "coverflow_slider".$x."_image", 9 );
								$slider_image = (isset($get_slider_image) ? $get_slider_image : '');
								
								$get_slider_texts = get_field( "coverflow_slider".$x."_description", 9 );
								$slider_texts = (isset($get_slider_texts) ? $get_slider_texts : '');
								
								if(!empty($slider_image) || !empty($slider_texts)) { ?>
									<li data-flip-title="jd-flipcover-<?php echo $x; ?>">
										<img src="<?php echo $slider_image; ?>" alt="" />
										<?php echo $slider_texts; ?>
									</li>
							<?php }
							}
						?>
					</ul>
				</div>
			</div>
			<div style="clear: both"></div>
		</div>
		<!--COVERFLOW CONTAINER END-->
		<div id="about-row-four" class="full-width">  
			<div id="testimonials" class="max-width">
				<h1 class="dark-blue-font">Testimonials</h1>
				<p id="quote-icon"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/quote.png" /></p>
				<?php echo do_shortcode('[widget id="testimonialrotatorwidget-2"]'); ?>
				<div style="clear: both"></div>
			</div>
			<div style="clear: both"></div>
		</div>
		<div id="home-row-five" class="full-width">  
			<div class="max-width"> 
				<h1 class="dark-blue-font">Awards</h1> 
				<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('home-awards')) : else : ?>
						<p><strong>Widget Ready</strong></p>  
				<?php endif; ?> 
				<div style="clear: both"></div>
			</div>
		</div>
	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>